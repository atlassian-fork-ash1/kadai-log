/* 
 * Copyright 2012 Atlassian PTY LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package kadai
package log

import scalaz.Show
import scalaz.syntax.id._
import scalaz.std.option._
import scalaz.std.string._

import org.apache.logging.log4j.ThreadContext.pop
import org.apache.logging.log4j.ThreadContext.push

/**
 * Defines a simple logging facility.
 *
 * To use simply mix in the Logging trait.
 * The standard Show type-class instances can
 * be imported by importing the Logging object:
 *
 * For example:
 * {{{
 * class MyClass extends Actor with Logging {
 * import Logging._
 *
 * info("MyClass is instantiated!")
 * …
 * }
 * }}}
 *
 * Note you will need an instance of the LogWriter
 * typeclass in implicit scope for any type you
 * want to log. The most convenient way to construct a
 * LogWriter is deriving it from a Show instance,
 * but a LogWriter can be derived for more structured
 * data, allowing for structured logging formats such as
 * JSON.
 *
 * Most commonly you may want Show[String],
 * for which you can `import scalaz.std.string._`
 */
trait Logger {
  protected def error[A: LogWriter](msg: => A): Unit
  protected def warn[A: LogWriter](msg: => A): Unit
  protected def info[A: LogWriter](msg: => A): Unit
  protected def withInfo[A: LogWriter, B](msg: => A)(f: => B): B
  protected def debug[A: LogWriter](msg: => A): Unit
  protected def withDebug[A: LogWriter, B](msg: => A)(f: => B): B
  protected def trace[A: LogWriter](msg: => A): Unit
  protected def withTrace[A: LogWriter, B](msg: => A)(f: => B): B
}

trait LoggingInstances {
  import scalaz._, Scalaz._

  private[log] def apply(cls: Class[_]) = org.apache.logging.log4j.LogManager.getLogger(cls)

  implicit def ShowSeq[S: Show] = new Show[Seq[S]] {
    override def shows(ts: Seq[S]) = ts.toList.shows
  }

  implicit val ShowString = Show[String]
}

object Logging extends LoggingInstances

trait Logging extends Logger {
  import Logging._

  /** allow syntax: log info "message" */
  protected val log = Logging(this.getClass)

  private def show[A: LogWriter](msg: => A) =
    log4j.Log4jMessage(message(msg))

  private def message[A: LogWriter](msg: => A): LogMessage[A] =
    implicitly[LogWriter[A]].apply(msg)

  override protected def error[A: LogWriter](msg: => A) =
    log.error { show(msg) }

  override protected def warn[A: LogWriter](msg: => A) =
    if (log.isWarnEnabled)
      log.warn { show(msg) }

  override protected def info[A: LogWriter](msg: => A) =
    if (log.isInfoEnabled)
      log.info { show(msg) }

  override protected def withInfo[A: LogWriter, B](msg: => A)(f: => B): B = {
    info(msg)
    f
  }

  override protected def debug[A: LogWriter](msg: => A) =
    if (log.isDebugEnabled)
      log.debug { show(msg) }

  override protected def withDebug[A: LogWriter, B](msg: => A)(f: => B): B = {
    debug(msg)
    f
  }

  override protected def trace[A: LogWriter](msg: => A) =
    if (log.isTraceEnabled)
      log.trace { show(msg) }

  override protected def withTrace[A: LogWriter, B](msg: => A)(f: => B): B = {
    trace(msg)
    f
  }

  def withContext[A: LogWriter, B](a: A)(f: => B): B = {
    push(message(a).toString)
    try f finally { pop; () }
  }

  @deprecated("use withInfo instead", "1.6")
  protected def withLog[A](s: String)(f: => A): A = {
    log.info(s)
    f
  }

  @deprecated("use withContext instead", "1.6")
  def withLogContext[A](s: String)(f: => A): A = {
    push(s)
    try f finally { pop; () }
  }
}
