package kadai.log

import scalaz.Show

/** Typeclass for producing structured logs */
trait LogWriter[A] {
  def apply(a: => A): LogMessage[A]
}

object LogWriter extends LogWriterInstances {
  def apply[A: LogWriter]: LogWriter[A] =
    implicitly[LogWriter[A]]
}

trait LogWriterInstances {
  /** Standard implementation just using a Show instance */
  implicit def show[A: Show]: LogWriter[A] =
    new LogWriter[A] {
      def apply(a: => A) =
        new LogMessage[A] {
          lazy val value = a
          override lazy val toString = Show[A].shows(value)
        }
    }
}