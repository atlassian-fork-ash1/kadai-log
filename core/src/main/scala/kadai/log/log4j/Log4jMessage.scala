package kadai.log
package log4j

/**
 * adapter for the Log4j Message class
 */
case class Log4jMessage[A](msg: LogMessage[A]) extends org.apache.logging.log4j.message.Message {

  override def getFormattedMessage: String =
    msg.toString

  override def getParameters: Array[AnyRef] =
    Array(ToAnyRef(msg.value))

  override val getFormat: String =
    ""

  override val getThrowable: Throwable =
    null
}

private[log4j] object ToAnyRef {
  // who said Java interop was easy?
  def apply[A](a: A): AnyRef =
    a match {
      case r: AnyRef  => r
      case i: Int     => i: java.lang.Integer
      case l: Long    => l: java.lang.Long
      case c: Char    => c: java.lang.Character
      case b: Byte    => b: java.lang.Byte
      case d: Double  => d: java.lang.Double
      case f: Float   => f: java.lang.Float
      case s: Short   => s: java.lang.Short
      case b: Boolean => b: java.lang.Boolean
    }
}