package kadai
package log

import scalaz.Show
import scala.reflect.ClassTag

/**

 * LogMessages get fed to loggers, allowing custom control of the message
 * String via the `toString`.
 * 
 * Normally, you just supply whatever Show instance for your data you want
 * to control the log output, but custom sub-types may implement structured
 * log formats such as JSON.
 */
trait LogMessage[A] {

  def value: A
}
