package kadai.log
package log4j

object Log extends Logging {
  import Logging._

  def foo(s: String) = log.error(Foo(s))
  def jsn(s: String) = JSON.foo(s)

}

object JSON extends json.JsonLogging {
  import json.JsonLogging._

  def foo(s: String) = error(Foo(s))
}
