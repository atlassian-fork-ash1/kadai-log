package kadai.log
package json

import argonaut.EncodeJson
import org.scalacheck.Prop
import org.specs2.{ Specification, ScalaCheck }
import scalaz.{ NonEmptyList, Show }
import scalaz.syntax.id._

object JsonLoggingSpec extends Specification with ScalaCheck with JsonLogging {
  import JsonLogging._

  def is = s2""" 
   JsonLogging should 
     standardise on 'msg' for a String                  $logString
     standardise on 'Throwable' for a Throwable         $logThrowable
     log both for a tuple                               $logTuple
     only log for a defined option                      $logOption
     prefer an explicit EncodeJson to a Show            $logWriterPrecedence
     default to class name for backwards compatibility  $logWriterDefaultQualifier
   """

  def logString =
    Prop.forAll { (s: String) =>
      LogWriter[String].apply(s) should beLike {
        case JsonMessage(_, List((q, ss))) => (q === "msg") and (ss === implicitly[EncodeJson[String]].encode(s))
      }
    }

  def logThrowable =
    Prop.forAll { (t: Throwable) =>
      LogWriter[Throwable].apply(t) should beLike {
        case JsonMessage(_, List((q, tt))) => (q === "Throwable") and (tt === implicitly[EncodeJson[Throwable]].encode(t))
      }
    }

  def logTuple =
    Prop.forAll { (s: String, t: Throwable) =>
      LogWriter[(String, Throwable)].apply(s -> t) should beLike {
        case JsonMessage(_, List((qs, ss), (qt, tt))) =>
          (qs === "msg") and (ss === implicitly[EncodeJson[String]].encode(s)) and
            (qt === "Throwable") and (tt === implicitly[EncodeJson[Throwable]].encode(t))
      }
    }

  def logOption =
    Prop.forAll { (s: Option[String]) =>
      LogWriter[Option[String]].apply(s) should beLike {
        case JsonMessage(_, List()) => s.isDefined === false
        case JsonMessage(_, List((_, ss))) => ss === implicitly[EncodeJson[String]].encode(s.get)
      }
    }

  case class Foo(i: Int)
  object Foo {
    implicit val FooEncoder: EncodeJson[Foo] =
      implicitly[EncodeJson[Int]].contramap { _.i }

    implicit val FooQualified = JsonMessage.Qualified.by[Foo]("foo")
  }
  def logWriterPrecedence = {
    LogWriter[Foo].apply(Foo(1)) should beLike {
      case JsonMessage(_, List((q, _))) => q === "foo"
    }
  }

  /** this needs to be outside of the test method otherwise an InternalError is thrown */
  case class Bar(i: Int)
  object Bar {
    implicit val FooEncoder: EncodeJson[Bar] =
      implicitly[EncodeJson[Int]].contramap { _.i }
  }

  def logWriterDefaultQualifier =
    LogWriter[Bar].apply(Bar(1)) should beLike {
      case JsonMessage(_, List((q, _))) => q === "Bar"
    }
}